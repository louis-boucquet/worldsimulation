const path = require("path");

module.exports = {
	// mode: "production",
	mode: "development",
	entry: './src/index.ts',
	output: {
		path: path.resolve(__dirname, "out")
	},
	resolve: {
		extensions: [".ts", ".js"]
	},
	module: {
		rules: [
			{ test: /\.tsx?$/, loader: "ts-loader" }
		]
	}
}
