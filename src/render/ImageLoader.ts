export function loadImage(url: string): Promise<HTMLImageElement> {
	const image = new Image();
	image.src = url;

	return new Promise(resolve => {
		image.onload = () => resolve(image)
	});
}

export default class ImageLoader {
	private images: { [key: string]: any } = {};
	private promises: { [key: string]: Promise<any> } = {};

	async getImage(url: string): Promise<HTMLImageElement> {
		const image = this.images[url];

		// return the image if it has been loaded
		if (image)
			return image;

		console.log("Image hasn't been loaded yet");

		// if the image is already loading, return it's promise
		const promise = this.promises[url];

		if (promise)
			return promise;

		console.log("Image hasn't started loading yet");

		// if the image isn't loaded and isn't loading make a new promise
		const newPromise = loadImage(url)
			.then(image => {
				delete this.promises[url];
				return this.images[url] = image;
			});

		this.promises[url] = newPromise;

		return newPromise;
	}

	getImages(urls: string[]) {
		return Promise.all(urls.map(url => this.getImage(url)));
	}
}
