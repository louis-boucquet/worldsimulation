import Coordinate from "./entities/Coordinate";
import Entity from "./entities/base/Entity";
import Movable from "./entities/base/Movable";
import Acceleratable from "./entities/base/Acceleratable";
import Renderer from "./Render/Renderer";
import Camera from "./Render/Camera";
import { registerRenderers } from "./Render/registerRenderers";
import Updatable from "./entities/Updatable";
import Tree from "./entities/derived/Tree";
import ImageLoader from "./render/ImageLoader";

const renderer = Renderer.getFullSizeCanvasRenderer();
const camera = Camera.getCanvasCamera();
camera.enableDraging();
camera.enableZooming();

// register renderers
registerRenderers(renderer);

const entities = new Set<Updatable>();

entities.add(new Tree(entities, new Coordinate(0, 0), 40));

const imageLoader = new ImageLoader();

update();

function update() {
	// Clear away last frame
	renderer.reset();
	// apply transformation
	renderer.setTransformation(camera);

	entities.forEach(entity => {
		entity.update();
		renderer.render(entity);
	})

	// apply reset transformation
	renderer.resetTransformation(camera);

	requestAnimationFrame(update);
}
