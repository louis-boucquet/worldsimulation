import Coordinate from "../Coordinate";
import Updatable from "../Updatable";
import Killable from "../Killable";
import Birther from "../Birter";

export default class Entity implements Updatable, Killable, Birther {
	constructor(
		public entities: Set<Entity>,
		private _location: Coordinate
	) {
	}

	public get location() {
		return this._location;
	}

	update() {
	}

	birth(entity: Entity) {
		this.entities.add(entity);
	}

	kill() {
		this.entities.delete(this);
	}
}