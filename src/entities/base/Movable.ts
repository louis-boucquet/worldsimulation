import Entity from "./Entity";
import Coordinate from "../Coordinate";

export default class Movable extends Entity {
	constructor(
		entities,
		location,
		public speed: Coordinate
	) {
		super(entities, location);
	}

	update() {
		this.location.add(this.speed)
		super.update();
	}
}