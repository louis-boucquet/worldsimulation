import Movable from "./Movable";
import Coordinate from "../Coordinate";

export default class Acceleratable extends Movable {
	constructor(
		entities,
		location,
		speed,
		public acceleration: Coordinate
	) {
		super(entities, location, speed);
	}

	update() {
		this.speed.add(this.acceleration);
		super.update();
	}
}