import Entity from "../base/Entity";
import Coordinate from "../Coordinate";

export default class Tree extends Entity {
	constructor(
		entities,
		location,
		private lifeTime: number
	) {
		super(entities, location);
	}

	update() {
		this.lifeTime--;

		if (this.lifeTime < 0) {
			this.kill();

			const space = [...this.entities]
				.filter(entity => entity !== this)
				.map(entity => Coordinate.dist(entity.location, this.location))
				.filter(dist => dist < 500)
				.length;

			// .map(dist => (500 - dist) / 500 )
			// .reduce((tot, curr) => tot + curr, 0);

			const amountOfChildren = (2 - space) * Math.random();

			for (let i = 0; i < amountOfChildren; i++) {
				this.birth(new Tree(this.entities, this.location.copy().addRandom(300), 300));
			}
		}

		super.update();
	}
}
