import Entity from "./base/Entity";

export default interface Birther {
	birth(entity: Entity);
}
