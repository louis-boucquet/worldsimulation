export default interface Killable {
	kill();
}