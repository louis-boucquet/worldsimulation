export default class Coordinate {
	constructor(
		private _x: number,
		private _y: number
	) {
	}

	public get x() {
		return this._x;
	}

	public get y() {
		return this._y;
	}

	add(amount: Coordinate) {
		this._x += amount.x;
		this._y += amount.y;

		return this;
	}

	sub(amount: Coordinate) {
		this._x -= amount.x;
		this._y -= amount.y;

		return this;
	}

	multiplyByNumber(amount: number) {
		this._x *= amount;
		this._y *= amount;

		return this;
	}

	addRandom(amount: number): Coordinate {
		this._x += (Math.random() * 2 - 1) * amount;
		this._y += (Math.random() * 2 - 1) * amount;

		return this;
	}

	copy() {
		return new Coordinate(this.x, this.y);
	}

	static dist(c1: Coordinate, c2: Coordinate) {
		const toSquare = Math.sqrt(c1.x - c2.x) + Math.sqrt(c1.y - c2.y)
		return toSquare * toSquare;
	}
}
