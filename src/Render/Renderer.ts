import Camera from "./Camera";

export default class Renderer<T> {
	private renderers: { [key: string]: ((toRender: any, t: T) => void) } = {}

	constructor(
		private t: T,
		private reseter: (t: T) => void
	) {
	}

	public reset() {
		this.reseter(this.t);
	}

	public setTransformation(camera: Camera<T>) {
		camera.setTransformation(this.t);
	}

	public resetTransformation(camera: Camera<T>) {
		camera.resetTransformation(this.t);
	}

	public render(toRender: any) {
		this.renderWithClassName(toRender.__proto__, toRender);
	}

	public renderWithClassName(type: any, toRender: any) {
		if (type) {
			const typeName = type.constructor.name;
			const renderer = this.renderers[typeName];

			if (renderer) {
				renderer(toRender, this.t);
			} else {
				const superClass = type.__proto__;
				this.renderWithClassName(superClass, toRender);
			}
		} else {
			throw "You can render an instance of type `Object`";
		}
	}

	public registerRenderer(type: string, renderer: (toRender: any, t: T) => void) {
		this.renderers[type] = renderer;
	}

	public static getFullSizeCanvasRenderer() {
		const canvas = document.createElement("canvas");
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
		document.body.appendChild(canvas);

		const ctx = canvas.getContext("2d");

		window.addEventListener("resize", e => {
			canvas.width = window.innerWidth;
			canvas.height = window.innerHeight;
		})

		return new Renderer(ctx, ctx => {
			ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height)
		});
	}
}