import Coordinate from "../entities/Coordinate";

export default class Camera<T> {
	private translation: Coordinate = new Coordinate(0, 0);
	private rotation: number = 0;
	private scale: number = 1;

	constructor(
		private setter: (t: T, camera: Camera<T>) => void,
		private resetter: (t: T, camera: Camera<T>) => void
	) {
	}

	resetTransformation(t: T) {
		this.resetter(t, this);
	}

	setTransformation(t: T) {
		this.setter(t, this);
	}

	translate(amount: Coordinate) {
		this.translation.add(amount);
	}

	changeScale(amount: number) {
		this.scale *= amount;
	}

	rotate(amount: number) {
		this.rotation += amount;
	}

	enableZooming(zoomSpeed = 1.003) {
		document.addEventListener("mousewheel", (e: any) => {
			const zoomAmount = Math.pow(zoomSpeed, -e.deltaY);

			const toTranslate = new Coordinate(e.x, e.y)
				.sub(this.translation)
				.multiplyByNumber(1 - zoomAmount);

			this.changeScale(zoomAmount);
			this.translate(toTranslate);
		});
	}

	enableDraging() {
		let last;
		let moving = false;

		document.addEventListener("mousedown", e => {
			e.preventDefault();

			last = new Coordinate(e.x, e.y);
			moving = true;

			// console.log("mouse clicked at", last);
		});

		document.addEventListener("mouseup", e => {
			e.preventDefault();

			last = new Coordinate(e.x, e.y);
			moving = false;

			// console.log("mouse released at", last);
		});

		document.addEventListener("mousemove", e => {
			e.preventDefault();

			if (moving) {
				const newLast = new Coordinate(e.x, e.y);
				// console.log("mouse dragging at", newLast);

				const toMove = newLast.sub(last);

				this.translate(toMove);

				last = new Coordinate(e.x, e.y);
			}
		});
	}

	static getCanvasCamera() {
		const camera = new Camera<CanvasRenderingContext2D>(
			(ctx, camera) => {
				// TRANSFORM
				ctx.translate(camera.translation.x, camera.translation.y)
				ctx.rotate(camera.rotation)
				ctx.scale(camera.scale, camera.scale)
			},
			(ctx, camera) => {
				// RESET
				ctx.scale(1 / camera.scale, 1 / camera.scale)
				ctx.rotate(-camera.rotation)
				ctx.translate(-camera.translation.x, -camera.translation.y)
			}
		);

		// center coordinate (0, 0)
		camera.translate(new Coordinate(window.innerWidth, window.innerHeight).multiplyByNumber(0.5))

		return camera;
	}
}
