import Entity from "../entities/base/Entity";
import Renderer from "./Renderer";

export function registerRenderers(renderer: Renderer<CanvasRenderingContext2D>) {
	renderer.registerRenderer(Entity.name, (toRender: Entity, ctx) => {
		ctx.fillStyle = "#5c8a21"
		ctx.beginPath();
		const { x, y } = toRender.location;
		ctx.arc(x, y, 25, 0, 2 * Math.PI);
		ctx.fill();
	});
}
